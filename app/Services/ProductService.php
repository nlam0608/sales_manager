<?php 

namespace App\Services;

use App\Repository\ProductRepository;

class ProductService
{
    protected $productRepository;
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function all()
    {
        return $this->productRepository->all();
    }
    public function getById($id)
    {
        return $this->productRepository->getById($id);
    }
    public function store($requests)
    {
        return $this->productRepository->store($requests);
    }
    public function destroy($id)
    {
        return $this->productRepository->destroy($id);
    }
    public function update($requests,$id)
    {
        return $this->productRepository->getById($id)->update($requests);
    }
}
