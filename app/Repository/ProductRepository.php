<?php

namespace App\Repository;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Pros\CodeBase\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{
    public $product;
    public function __construct(Product $product) 
    {
        parent::__construct();
        return $this->product = $product;
    }
    public function all(){
        return $this->get();
    }
    public function getById($id){
        return $this->findOrFail($id);
    }
    public function store($repuest){
        return $this->create($repuest);
    }
    public function update($repuest , $id){
        $product = $this->getById($id);
        return $product->update($repuest);
    }
    public function destroy($id){
        $product = $this->getById($id);
        return $product->delete();
    }
}
