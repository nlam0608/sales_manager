<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $product = new Product();
        $product->name = 'sản phảm a';
        $product->category = 'Sản phẩm mới';
        $product->price = 1231;
        $product->quantity = 1;
        $product->image = 'ádf';
        $product->status = 'Active';
        $product->save();
    }
}
